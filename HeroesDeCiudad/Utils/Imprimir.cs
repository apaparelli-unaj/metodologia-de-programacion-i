/*
    Imprimir.
    Contiene funciones para formateo de impresiones.
*/
using System;


namespace HeroesDeCiudad{

	/// <summary>
	/// Clase Imprimir.
	/// </summary>
    class Imprimir {
		
		/// <summary>
		/// Método para imprimir una matriz con filas y columnas.
		/// <example>Ejemplo de uso:
		/// <code>
		///    n = Imprimir.imprimir_matriz(matriz);
		/// </code>
		/// </example>
		/// </summary>
		/// <param><c>matriz</c> matriz de la forma int[][].</param>
        public static void imprimir_matriz(int[][] matriz){
            for (int i = 0; i < matriz.GetLength(0); i++){
                for (int j = 0; j < matriz.GetLength(0); j++){
                    Console.Write("{0,-4:D3}", matriz[i][j] + " ");
                }
                Console.WriteLine();
            }
        }

		/// <summary>
		/// Método para imprimir una línea de puntos a modo de separador.
		/// <example>Ejemplo de uso:
		/// <code>
		///    n = Imprimir.separador();
		/// </code>
		/// </example>
		/// </summary>
        public static void separador(){
            Console.WriteLine(".....................................");
        }

    }
}
