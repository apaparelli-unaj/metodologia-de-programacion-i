/*
    Utils.
    Contiene funciones para ser reutilizadas.
*/
using System;

namespace HeroesDeCiudad{
	
	/// <summary>
	/// Clase Utils.
	/// </summary>
    class Utils {

		/// <summary>
		/// Método para obtener el tamaño de una matriz según su superficie.
		/// <example>Ejemplo de uso:
		/// <code>
		///	   int superficie = 50;
		///    n = Utils.getCantidadElementos(superficie);
		/// </code>
		/// </example>
		/// </summary>
		/// <param><c>superficie</c> Valor de la superficie.</param>
		/// <returns>retorna el rango de la matriz. Cantidad de filas / columnas.</returns>
        public static int getCantidadElementos(double superficie){
            double raiz = Math.Sqrt(superficie);
            int n = Convert.ToInt32(Math.Round(raiz));
            return n;
        }

		/// <summary>
		/// Método para completar una matriz con valores al azar entre 0 y 100.
		/// <example>Ejemplo de uso:
		/// <code>
		///	   int superficie = 50;
		///    sectores = Utils.completarSectores(superficie);
		/// </code>
		/// </example>
		/// </summary>
		/// <param><c>superficie</c> Valor de la superficie.</param>
		/// <returns>retorna matriz con datos al azar.</returns>
        public static int[][] completarSectores(double superficie){
            Random r = new Random();
            int n = getCantidadElementos(superficie);

            int[][] sectores = new int[n][];
            for (int i = 0; i < n; i++){
                sectores[i] = new int[n];
                for (int j = 0; j < n; j++){
                    int valor = r.Next(101);
                    sectores[i][j] = valor;
                }
            }

            return sectores;
        }

    }
}
