/*
    Casa.
    Contiene clase y métodos relacionado a casas.
*/
using System;


namespace HeroesDeCiudad{

	/// <summary>
	/// Clase Casa.
	/// Implementa la interfaz Ilugar. 
	/// </summary>
    public class Casa : ILugar{

        public int numeroPuerta { get; set; }
        public double superficie { get; set; }
        public int cantidadHabitantes { get; set; }
        // El atributo sectores es usado para guardar la afectación del fuego de cada sector. 
        private int[][] sectores;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <example>Ejemplo de uso:
		/// <code>
		///    Casa casa = new Casa(312, 75, 3);
		/// </code>
		/// </example>
		/// </summary>
		/// <param><c>p</c> Numero de puerta de la casa.</param>
        /// <param><c>s</c> Superficie de la casa.</param>
        /// <param><c>h</c> Cantidad de habitantes en la casa.</param>
        public Casa(int p, int s, int h){
            numeroPuerta = p;
            superficie = s;
            cantidadHabitantes = h;
            // Se genera el contenido de los sectores.
            sectores = Utils.completarSectores(superficie);
        }

		/// <summary>
		/// Método para obtener la matriz sectores.
		/// </summary>
        public int[][] getSectores(){
            return this.sectores; 
        }

    }
}
