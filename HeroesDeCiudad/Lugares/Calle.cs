/*
    Calle.
    Contiene clase y métodos relacionado a calles
*/
using System;


namespace HeroesDeCiudad{

	/// <summary>
	/// Clase Calle.
	/// </summary>
    public class Calle {

        public double longitud { get; set; }
        public int cantidadFarolas { get; set; }
        public int caudalAgua { get; set; }

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <example>Ejemplo de uso:
		/// <code>
		///    Calle calle1 = new Calle(99.9 , 9, 45);
		/// </code>
		/// </example>
		/// </summary>
		/// <param><c>l</c> Longitud de la calle</param>
        /// <param><c>f</c> Cantidad de farolas en la calle.</param>
        /// <param><c>a</c> Caudal de agua de la calle.</param>
        public Calle(double l, int f, int a){
            longitud = l;
            cantidadFarolas = f;
            caudalAgua = a;
        }

    }
}
