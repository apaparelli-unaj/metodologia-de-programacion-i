/*
    Plaza.
    Contiene clase y métodos relacionado a bomberos
*/
using System;


namespace HeroesDeCiudad{

	/// <summary>
	/// Clase Plaza.
	/// Implementa la interfaz Ilugar. 
	/// </summary>
    public class Plaza : ILugar{

        public string nombre { get; set; }
        public double superficie { get; set; }
        public int cantidadArboles { get; set; }
        public int cantidadFarolas { get; set; }
        // El atributo sectores es usado para guardar la afectación del fuego de cada sector. 
        private int[][] sectores;

		/// <summary>
		/// Constructor.
		/// <example>Ejemplo de uso:
		/// <code>
		///    Plaza plaza = new Plaza("San Martin", 40 , 30, 20);
		/// </code>
		/// </example>
		/// </summary>
		/// <param><c>n</c> Nombre de la plaza.</param>
        /// <param><c>s</c> Superficie de la plaza.</param>
        /// <param><c>a</c> Cantidad de árboles en la plaza.</param>
        /// <param><c>f</c> Cantidad de farolas en la plaza.</param>
        public Plaza(string n, double s, int a, int f){
            nombre = n;
            superficie = s;
            cantidadArboles = a;
            cantidadFarolas = f;
            // Se genera el contenido de los sectores.
            sectores = Utils.completarSectores(superficie);
        }

		/// <summary>
		/// Método para obtener la matriz sectores.
		/// </summary>
        public int[][] getSectores(){
            return this.sectores; 
        }

    }
}
