/*
    Esquina.
    Contiene clase y métodos relacionado a bomberos
*/
using System;


namespace HeroesDeCiudad{
	
	/// <summary>
	/// Clase  Esquina. 
	/// </summary>
    public class Esquina {

        public int cantidadSemaforos { get; set; }

		/// <summary>
		/// Constructor
		/// </summary>
		/// <example>Ejemplo de uso:
		/// <code>
		///    Esquina esquina1 = new Esquina(3);
		/// </code>
		/// </example>
		/// </summary>
		/// <param><c>s</c> Cantidad de semaforos en la esquina.</param>
        public Esquina(int s){
            cantidadSemaforos = s;
        }

    }
}
