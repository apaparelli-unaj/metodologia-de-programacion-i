/*
    ILugar.
    Contiene Interfaz y firmas para clases que la implementen.
*/
using System;


namespace HeroesDeCiudad{

	/// <summary>
	/// Interfaz  ILugar. 
	/// Contiene las firmas de los métodos.
	/// </summary>
    public interface ILugar {
        int[][] getSectores();
    }

}
