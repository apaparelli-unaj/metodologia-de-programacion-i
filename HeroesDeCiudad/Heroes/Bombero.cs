/*
    Bombero.
    Contiene interfaz, clase y métodos relacionado a bomberos
*/
using System;


namespace HeroesDeCiudad{

	/// <summary>
	/// Interfaz  IBomberos. 
	/// Contiene las firmas de los métodos.
	/// </summary>
    public interface IBombero {
        void apagarIncendio();
        void apagarIncendio(ILugar lugar, Calle calle);
        void bajarGatitoDeArbol();
    }

	/// <summary>
	/// Clase Bombero.
	/// </summary>
    public class Bombero : IBombero {
		
		// Se define la estrategia Escalera como default.
        private IEstrategiaApagarIncendio estrategia = new Escalera();

		/// <summary>
		/// Método con acciones para apagar incendio.
		/// Imprime mensaje de Práctica 1
		/// </summary>
        public void apagarIncendio(){
            Console.WriteLine("Apagar incendio...");
        }

		/// <summary>
		/// Método con acciones para apagar incendio.
		/// Se ejecuta el apagado de incendio invocando a la estrategía elegida.
		/// </summary>
        public void apagarIncendio(ILugar lugar, Calle calle){
            estrategia.apagarIncendio(lugar, calle);
        }

		/// <summary>
		/// Método con acciones para bajar al gatito del árbol.
		/// Imprime mensaje de Práctica 1
		/// </summary>
        public void bajarGatitoDeArbol(){
            Console.WriteLine ("Bajar Gatito de arból...");
        }

		/// <summary>
		/// Método para cambiar la estrategia.
		/// <example>Ejemplo de uso:
		/// <code>
		///    Bombero b = new Bombero();
		///    b.setEstrategia("escalera");
		/// </code>
		/// </example>
		/// </summary>
		/// <param><c>opcion</c> key para la estrategia.</param>
        public void setEstrategia(string opcion){
            switch (opcion){
                case "secuencial":
                    this.estrategia = new Secuencial();
                    break;
                case "escalera":
                    this.estrategia = new Escalera();
                    break;
                case "espiral":
                    this.estrategia = new Espiral();
                    break;
            }
        }

		/// <summary>
		/// Método para imprimir la secuencia de apagado de incendio de un sector específico.
		/// </summary>
		/// <param><c>fila</c> Número de fila del sector.</param>
		/// <param><c>columna</c> Número de columna del sector.</param>
		/// <param><c>afectacion</c> Porcentaje de afectación de fuego para ese sector.</param>
		/// <param><c>caudal</c> Caudal de agua de la calle.</param>
        public static int apagarRecursivo(int fila, int columna, int afectacion, int caudal ){
			Console.Write("({0},{1})", fila, columna);
            int nuevo_caudal = Bombero.apagarRecursivoResolver(afectacion, caudal);
            Console.WriteLine();
            return nuevo_caudal;
        }

		/// <summary>
		/// Método recursivo para de apagado de incendio de un sector específico.
		/// Se va restando a la cantidad de afectación el caudal. Cuando la afectación
		/// Es menor al caudal, lo marca en cero, y queda extinguido el fuego.
		/// </summary>
		/// <param><c>afectacion</c> Porcentaje de afectación de fuego para ese sector.</param>
		/// <param><c>caudal</c> Caudal de agua de la calle.</param>
		/// <returns>
		///	Devuelve 0 si llego al fina de la recursion.
		/// Devuelve una llamada a si mismo mientras no sea cero la afectación.
		///</returns>
        public static int apagarRecursivoResolver(int afectacion, int caudal){
			Console.Write(" -> {0}", afectacion);

            if (afectacion == 0){
                return 0; 
            }

            if (afectacion > caudal){
                afectacion = afectacion - caudal; 
            }
            else{
                afectacion = 0;
            }

            return apagarRecursivoResolver(afectacion, caudal);
        }

    }
}
