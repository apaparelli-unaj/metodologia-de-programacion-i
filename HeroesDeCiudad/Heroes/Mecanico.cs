/*
    Mecánico.
    Contiene interfaz, clase y métodos relacionado a mécanicos
*/
using System;


namespace HeroesDeCiudad {

	/// <summary>
	/// Interfaz  IMecanico. 
	/// Contiene las firmas de los métodos.
	/// </summary>
    public interface IMecanico {
        void arreglarMotor();
        void remolcarAuto();
    }

	/// <summary>
	/// Clase Mecanico.
	/// </summary>
    public class Mecanico : IMecanico {

		/// <summary>
		/// Método con acciones para arreglar motor.
		/// Imprime mensaje de Práctica 1
		/// </summary>
        public void arreglarMotor(){
            Console.WriteLine("Arreglando motor...");
        }

		/// <summary>
		/// Método con acciones para remolcar auto.
		/// Imprime mensaje de Práctica 1
		/// </summary>
        public void remolcarAuto(){
            Console.WriteLine ("Remolcando Auto...");
        }

    }
}
