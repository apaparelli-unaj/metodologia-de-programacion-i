/*
    Medico.
    Contiene interfaz, clase y métodos relacionado a médicos
*/

using System;


namespace HeroesDeCiudad{

	/// <summary>
	/// Interfaz  IMedico. 
	/// Contiene las firmas de los métodos.
	/// </summary>
    public interface IMedico {
        void atenderInfarto();
        void atenderDesmayo();
    }

	/// <summary>
	/// Clase Medico.
	/// </summary>
    public class Medico : IMedico {
		
		/// <summary>
		/// Método con acciones para patrullar calle.
		/// Imprime mensaje de Práctica 1
		/// </summary>
        public void atenderInfarto(){
            Console.WriteLine("Atender infarto...");
        }

		/// <summary>
		/// Método con acciones para patrullar calle.
		/// Imprime mensaje de Práctica 1
		/// </summary>
        public void atenderDesmayo(){
            Console.WriteLine ("Atender desmayo...");
        }

    }
}
