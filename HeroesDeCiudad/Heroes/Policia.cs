/*
    Policia.
    Contiene interfaz, clase y métodos relacionado a policias
*/
using System;


namespace HeroesDeCiudad{

	/// <summary>
	/// Interfaz  IPolicia. 
	/// Contiene las firmas de los métodos.
	/// </summary>
    public interface IPolicia {
        void patrullarCalles();
        void detenerDelincuente();
    }

	/// <summary>
	/// Clase Policia.
	/// </summary>
    public class Policia : IPolicia {

		/// <summary>
		/// Método con acciones para patrullar calle.
		/// Imprime mensaje de Práctica 1
		/// </summary>
        public void patrullarCalles(){
            Console.WriteLine("Patrullar calles...");
        }

		/// <summary>
		/// Método con acciones para detener delincuente.
		/// Imprime mensaje de Práctica 1
		/// </summary>
        public void detenerDelincuente(){
            Console.WriteLine ("Detener delincuente...");
        }

    }
}
