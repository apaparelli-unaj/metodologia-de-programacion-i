/*
    Electricista.
    Contiene interfaz, clase y métodos relacionado a electricistas
*/
using System;


namespace HeroesDeCiudad {

	/// <summary>
	/// Interfaz  IElectricista. 
	/// Contiene las firmas de los métodos.
	/// </summary>
    public interface IElectricista {
        void revisar();
    }

	/// <summary>
	/// Clase Electricista.
	/// </summary>
    public class Electricista : IElectricista {

		/// <summary>
		/// Método con acciones para revisar luces.
		/// Imprime mensaje de Práctica 1
		/// </summary>
        public void revisar(){
            Console.WriteLine("Electricista revisando...");
        }

    }
}
