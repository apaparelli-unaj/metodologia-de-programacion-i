﻿using System;

namespace HeroesDeCiudad {

    class Practica2 {

        public Practica2() {
            Console.WriteLine("###### Inicio tests Práctica 2 ######");
            Imprimir.separador();
            TestCasaApagarIncendio();
            Imprimir.separador();
            TestPlazaApagarIncendio();
            Imprimir.separador();
            Console.WriteLine("####### Fin tests Práctica 2 #######");
        }

        private static void TestCasaApagarIncendio(){
            Bombero bombero1 = new Bombero();
            Casa casa = new Casa(312, 75, 3);
            Calle calle = new Calle(99.9 , 9, 50);

			Console.WriteLine("Sectores de casa al inicio");
            Imprimir.imprimir_matriz(casa.getSectores());
			Console.WriteLine("Caudal de la calle: {0}", calle.caudalAgua);

            bombero1.setEstrategia("escalera");
            bombero1.apagarIncendio(casa, calle);

			Console.WriteLine("Sectores de casa al finalizar");
            Imprimir.imprimir_matriz(casa.getSectores());
        }

        private static void TestPlazaApagarIncendio(){
            Bombero bombero1 = new Bombero();
            Plaza plaza = new Plaza("San Martin", 40 , 30, 20);
            Calle calle = new Calle(99.9 , 9, 30);

			Console.WriteLine("Sectores de plaza al inicio");
            Imprimir.imprimir_matriz(plaza.getSectores());
			Console.WriteLine("Caudal de la calle: {0}", calle.caudalAgua);

            bombero1.setEstrategia("espiral");
            bombero1.apagarIncendio(plaza, calle);

			Console.WriteLine("Sectores de plaza al finalizar");
            Imprimir.imprimir_matriz(plaza.getSectores());
        }

    }
}
