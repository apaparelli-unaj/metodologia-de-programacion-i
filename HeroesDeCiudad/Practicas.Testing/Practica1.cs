﻿using System;

namespace HeroesDeCiudad {

    class Practica1 {

        public Practica1() {
            Console.WriteLine("###### Inicio tests Práctica 1 ######");
            Imprimir.separador();
            TestBomberoMethods();
            Imprimir.separador();
            TestMedicoMethods();
            Imprimir.separador();
            TestPoliciaMethods();
            Imprimir.separador();
            TestElectricistaMethods();
            Imprimir.separador();
            TestMecanicoMethods();
            Imprimir.separador();
            TestCasaProperties();
            Imprimir.separador();
            TestEsquinaProperties();
            Imprimir.separador();
            TestPlazaProperties();
            Imprimir.separador();
            TestCalleProperties();
            Imprimir.separador();
            Console.WriteLine("####### Fin tests Práctica 1 #######");
        }


        private static void TestBomberoApagarIncendio(){
            Bombero bombero1 = new Bombero();
            Plaza plaza = new Plaza("San Martin", 40 , 30, 20);
            Calle calle = new Calle(99.9 , 9, 1000);
            Console.WriteLine("Sectores Plaza");
            int[][] sectores = plaza.getSectores();
            Imprimir.imprimir_matriz(sectores);

            bombero1.apagarIncendio(plaza, calle);
            bombero1.apagarIncendio();
            bombero1.bajarGatitoDeArbol();
        }

        private static void TestBomberoMethods(){
            Bombero bombero1 = new Bombero();
            bombero1.apagarIncendio();
            bombero1.apagarIncendio();
            bombero1.bajarGatitoDeArbol();
        }

        private static void TestMedicoMethods(){
            Medico medico1 = new Medico();
            medico1.atenderInfarto();
            medico1.atenderDesmayo();
        }

        private static void TestPoliciaMethods(){
            Policia policia1 = new Policia();
            policia1.patrullarCalles();
            policia1.detenerDelincuente();
        }

        private static void TestElectricistaMethods(){
            Electricista electricista1 = new Electricista();
            electricista1.revisar();
        }

        private static void TestMecanicoMethods(){
            Mecanico mecanico1 = new Mecanico();
            mecanico1.arreglarMotor();
            mecanico1.remolcarAuto();
        }

        private static void TestCasaProperties(){
            Casa casa1 = new Casa(312, 75, 3);
            casa1.numeroPuerta = 265;
            Console.WriteLine("Número de puerta: " + casa1.numeroPuerta);
            Console.WriteLine("Superficie: " + casa1.superficie + " M2.");
            Console.WriteLine("Cantidad de habitantes: " + casa1.cantidadHabitantes);
        }

        private static void TestEsquinaProperties(){
            Esquina esquina1 = new Esquina(3);
            esquina1.cantidadSemaforos = 2;
            Console.WriteLine("Cantidad de Semaforos: " + esquina1.cantidadSemaforos);
        }

        private static void TestPlazaProperties(){
            Plaza plaza1 = new Plaza("San Martin", 75.5 , 30, 20);
            Console.WriteLine("Nombre: " + plaza1.nombre);
            Console.WriteLine("Superficie: " + plaza1.superficie + " M2.");
            Console.WriteLine("Cantidad de Arboles: " + plaza1.cantidadArboles);
            Console.WriteLine("Cantidad de Farolas: " + plaza1.cantidadFarolas);
        }

        private static void TestCalleProperties(){
            Calle calle1 = new Calle(99.9 , 9, 1000);
            Console.WriteLine("Longitud: " + calle1.longitud);
            Console.WriteLine("Cantidad de Farolas: " + calle1.cantidadFarolas);
            Console.WriteLine("Caudal de agua: " + calle1.caudalAgua + " Litros por minuto");
        }

    }
}
