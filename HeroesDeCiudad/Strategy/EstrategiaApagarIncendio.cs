/*
    Estrategia de apagado de incendios.
    Contiene interfaz de estrategias.
*/
using System;


namespace HeroesDeCiudad{

	/// <summary>
	/// Interfaz  IEstrategiaApagarIncendio. 
	/// Contiene las firmas de los métodos.
	/// </summary>
    public interface IEstrategiaApagarIncendio {
        void apagarIncendio(ILugar lugar, Calle calle);
    }
}



