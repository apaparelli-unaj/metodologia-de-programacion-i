/*
    Estrategia Espiral.
    Contiene la clase y métodos relacionado al apagado de incendios de forma espiralada.
*/
using System;

namespace HeroesDeCiudad{

	/// <summary>
	/// Clase Espiral.
	/// Implementa la interfaz IEstrategiaApagarIncendio. 
	/// </summary>
	public class Espiral: IEstrategiaApagarIncendio {

		/// <summary>
		/// Método para apagar el incendio en espiral.
		/// <example>Ejemplo de uso:
		/// <code>
		///    Bombero bombero = new Bombero();
		///    Casa casa = new Casa(312, 75, 3);
		///    Calle calle = new Calle(99.9 , 9, 50);
		///    bombero.setEstrategia("espiral");
		///    bombero.apagarIncendio(casa, calle);
		/// </code>
		/// </example>
		/// <example>Ejemplo de recorrido:
		/// <code>
		///    01  02  03  04
		///    12  13  14  05
		///    11  16  15  06
		///	   10  09  08  07
		/// </code>
		/// </example>
		/// </summary>
        /// <param><c>lugar</c> instancia de clase que implementa ILugar.</param>
        /// <param><c>calle</c> Instancia de calle.</param>
		public void apagarIncendio(ILugar lugar, Calle calle){
			int [][] matriz = lugar.getSectores();
            int n = matriz.GetLength(0);
			int c = 1, i, inicio = 0, limite = n - 1;

			Console.WriteLine("Apagando Incendio - Espiral");

			while(c <= n * n){
				for(i=inicio; i<=limite; i++){
			        matriz[inicio][i] = Bombero.apagarRecursivo(inicio, i, matriz[inicio][i], calle.caudalAgua);
				}
				for(i = inicio + 1; i <= limite; i++){
			        matriz[i][limite] = Bombero.apagarRecursivo(i,limite, matriz[i][limite], calle.caudalAgua);
				}
				for(i=limite-1; i>=inicio; i--){
			        matriz[limite][i] = Bombero.apagarRecursivo(limite,i, matriz[limite][i], calle.caudalAgua);
				}
				for(i=limite-1; i>inicio; i--){
			        matriz[i][inicio] = Bombero.apagarRecursivo(i, inicio, matriz[i][inicio], calle.caudalAgua);
				}

				inicio = inicio + 1;
				limite = limite - 1;
				c = c + 1;
			}

			Console.WriteLine("¡¡¡¡¡¡¡ El fuego de {0} fue extinguido en su totalidad !!!!!!", lugar.ToString());
		}

	}
}
