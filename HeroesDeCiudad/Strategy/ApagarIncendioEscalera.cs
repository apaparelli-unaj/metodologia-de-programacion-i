/*
    Estrategia Escalera.
    Contiene la clase y métodos relacionado al apagado de incendios de forma de escalera.
*/
using System;


namespace HeroesDeCiudad{

	/// <summary>
	/// Clase Escalera.
	/// Implementa la interfaz IEstrategiaApagarIncendio. 
	/// </summary>
	public class Escalera: IEstrategiaApagarIncendio {

		/// <summary>
		/// Método para apagar el incendio en escalera.
		/// <example>Ejemplo de uso:
		/// <code>
		///    Bombero bombero = new Bombero();
		///    Casa casa = new Casa(312, 75, 3);
		///    Calle calle = new Calle(99.9 , 9, 50);
		///    bombero.setEstrategia("escalera");
		///    bombero.apagarIncendio(casa, calle);
		/// </code>
		/// </example>
		/// <example>Ejemplo de recorrido:
		/// <code>
		///    01  02  03  04
		///    08  07  06  05
		///    09  10  11  12
		///	   16  15  14  13
		/// </code>
		/// </example>
		/// </summary>
        /// <param><c>lugar</c> instancia de clase que implementa ILugar.</param>
        /// <param><c>calle</c> Instancia de calle.</param>
		public void apagarIncendio(ILugar lugar, Calle calle){
			int [][] matriz = lugar.getSectores();

			Console.WriteLine("Apagando Incendio - Escalera");
            int n = matriz.GetLength(0);

            for (int i = 0; i < n; i++){
				if (i%2 == 0){
                    for (int j = 0; j < n ; j++){
                        matriz[i][j] = Bombero.apagarRecursivo(i, j, matriz[i][j], calle.caudalAgua);
                    }
                }
				else{
                    for (int j = n-1; j >= 0; j--){
                        matriz[i][j] = Bombero.apagarRecursivo(i, j, matriz[i][j], calle.caudalAgua);
                    }
				}
				
            }
 
			Console.WriteLine("¡¡¡¡¡¡¡ El fuego de {0} fue extinguido en su totalidad !!!!!!", lugar.ToString());
		}
	}
}
