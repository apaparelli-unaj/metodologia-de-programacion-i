﻿using System;

namespace HeroesDeCiudad {

    class Program {
    	static void Main(string[] args) {
			Console.WriteLine("Opción (0) Todas las prácticas.");
			Console.WriteLine("Opción (1) Práctica 1 - Introducción.");
			Console.WriteLine("Opción (2) Práctica 2 - Strategy.");
           	Console.WriteLine("Seleccione una opción: ");

            switch (Console.Read()){
                case '0':
            		new Practica1();
					new Practica2();
                    break;
                case '1':
            		new Practica1();
                    break;
                case '2':
            		new Practica2();
                    break;
            } 
            Console.ReadKey();
        }
    }
}
