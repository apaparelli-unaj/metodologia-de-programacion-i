# Metodología de programación I

Materia Metodología de programación I - Patrones de Diseño

Juego "Heroes de ciudad¨ 


## Propósito

Desarrollar un sistema donde distintos héroes solucionan diversos problemas que suceden en
una ciudad.

## Héroes

Los distintos héroes que intervienen en los diferentes problemas de una ciudad son:

**Bombero**: apagan incendios
**Médico**: atienden emergencias de salud
**Policía**: arrestan a los ladrones
**Electricista**: solucionan desperfectos eléctricos
**Mecánico**: arreglan autos

En la ciudad existen distintos lugares donde aparecen los problemas:

**Casas**. Pueden ocurrir incendios, problemas eléctricos, robos y sus habitantes sufrir
algún problema de salud
**Esquinas**. Pueden ocurrir robos, problemas eléctricos, los transeúntes sufrir
problemas de salud.
**Plazas**. Pueden ocurrir incendios, problemas eléctricos y robos.
**Calles**. Pueden ocurrir problemas eléctricos y los autos pueden sufrir desperfectos.

## Patrones utilizados

Para el desarrollo del juego se utilizan distintos patrones:

1. **Strategy**: Implementa diferentes estrategias de apagado de incendios para los bomberos.
2. **Observer**: Implementa un sistema de alarma donde los bomberos puedan enterarse de cada nuevo foco
de incendio.
3. **Composite**: Implementa la capacidad de logística para que un electricista pueda revisar de manera ágil y
transparente las farolas y semáforos de la ciudad reemplazando las lámparas quemadas.
4. **Decorator**: Implementar la particularidad de agregar “dificultades” al apagado de incendios de plazas por
parte de los bomberos.
5. **Command**: Implementa distintas acciones a realizar al momento de avistar un intento de
robo para los policías.


Mas información en la [wiki del proyecto](https://gitlab.com/apaparelli-unaj/metodologia-de-programacion-i/wikis/home)